'use strict';
import r from './../libs/rethink.js';
import config from './../config/db.js';

export function up(next) {
  r.dbCreate(config.db)
    .run(next);
};

export function down(next) {
  r.dbDrop(config.db)
    .run(next);
};
