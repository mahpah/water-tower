import 'babel-polyfill';
import request from 'co-supertest';
import 'co-mocha';
import app from './../../app';
import {User} from './../../app/models/user.js';
import {expect} from 'chai';

let server = app.listen();

/**
 * Assume user models is completed.
 */

describe('Api authenticaion', function() {
  let userId;
  let username = 'kristin';
  let password = 'secret';

  before(function*() {
    let user = new User({username, password});
    yield user.save();
    userId = user.id;
  });

  after(function*() {
    yield User.remove(userId);
  });

  describe('Generate access token', () => {
    it('should provide user access token', function*() {
      yield request.agent(server)
        .post('/authenticate')
        .set('Accept', 'application/json')
        .send({
          username,
          password,
        })
        .expect(200)
        .expect(({body}) => {
          expect(body.access_token).to.exist;
          expect(body.username).to.exist;
          expect(body.username).to.equal(username);
          expect(body.id).to.exist;
        })
        .end();
    });

    it('should not authenticate user with wrong password', function* () {
      yield request.agent(server)
        .post('/authenticate')
        .set('Accept', 'application/json')
        .send({
          username,
          password: password + 'asrts',
        })
        .expect({
          message: 'Wrong username or password',
        })
        .end();
    });
  });

  describe('Using access token', () => {
    let token;
    let userId;

    before(function* () {
      yield request.agent(server)
        .post('/authenticate')
        .set('Accept', 'application/json')
        .send({
          username,
          password,
        })
        .expect(200)
        .expect(({body}) => {
          expect(body.access_token).to.exist;
          token = body.access_token;
          userId = body.id;
        })
        .end();
    });

    it('should verify access token', function* () {
      yield request.agent(server)
        .get(`/me`)
        .set('Authorization', `Bearer ${token}`)
        .set('Content-Type', 'application/json')
        .expect(200)
        .expect(({body}) => {
          expect(body.valid).to.be.true;
          expect(body.username).to.equal(username);
          expect(body.id).to.equal(userId);
        })
        .end();
    });

    it('should refresh access token', function* () {
      yield request.agent(server)
        .get(`/refresh_token?access_token=${token}`)
        .set('Content-type', 'application/json')
        .expect(200)
        .expect(({body}) => {
          expect(body.access_token).to.exist;
          expect(body.username).to.equal(username);
          expect(body.id).to.equal(userId);
        })
        .end();
    });
  });

});
