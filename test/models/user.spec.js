import 'babel-polyfill';
import {expect} from 'chai';
import 'co-mocha';
import {User, TableName} from '../../app/models/user';
import r from './../../app/libs/rethink.js';

describe('User model', () => {

  after(function* () {
    try {
      yield r.table(TableName)
        .delete()
        .run();
    } catch (e) {
      console.log('Error when clear', e);
    }
  });

  beforeEach(function* () {
    yield r.table(TableName)
      .delete()
      .run();
  });

  it('should create user', function* () {
    let user = new User();
    expect(typeof user).to.equal('object');
  });

  it('should store user data', function* () {
    let user = new User({name: 'john'});
    expect(user.name).to.equal('john');
  });

  it('should create user id', function* () {
    let username = 'john';
    let password = 'secret';
    let user = new User({username, password,});
    yield user.save();
    expect(user.id).to.exist;
  });

  it('should find user by username', function* () {
    let username = 'john';
    let password = 'secret';
    let user = new User({username, password,});
    yield user.save();

    let userFound = yield User.findByUsername(username);
    expect(userFound.username).to.equal(username);

    let doe = yield User.findByUsername('doe');
    expect(doe).not.to.exist;
  });

  it('shoud update userdata', function* () {
    let johnName = 'john';
    let kristinName = 'kristin';
    let password = 'secret';
    let john;
    let kristin;
    let user = new User({username: johnName, password,});
    yield user.save();

    john = yield User.findByUsername(johnName);
    expect(john).to.exist;

    user.username = kristinName;
    yield user.save();

    john = yield User.findByUsername(johnName);
    kristin = yield User.findByUsername(kristinName);
    expect(john).not.to.exist;
    expect(kristin).to.exist;
  });

  it('should create user password hash when save', function* () {
    let username = 'john';
    let password = 'secret';
    let user = new User({username, password});
    yield user.save();
    expect(user.password).not.to.equal(password);
  });

  it('should validate correct password', function* () {
    let username = 'john';
    let password = 'secret';
    let user = new User({username, password});
    yield user.save();

    expect(yield user.isPassword(password)).to.be.true;
  });

  it('should not validate incorrect password', function* () {
    let username = 'john';
    let password = 'secret';
    let user = new User({username, password});
    yield user.save();

    expect(yield user.isPassword('password000')).to.be.false;
  });

  it('shoud get user by id', function* () {
    let username = 'john';
    let password = 'secret';
    let user = new User({username, password});
    yield user.save();

    let john = yield User.get(user.id);
    expect(john instanceof User).to.be.true;
    expect(john.username).to.eql(user.username);
  });

  it('shoud remove user', function* () {
    let username = 'john';
    let password = 'secret';
    let user = new User({username, password});
    yield user.save();

    let result = yield User.remove(user.id);
    expect(result).to.be.true;
    expect(yield User.get(user.id)).not.to.exist;
  });

});
