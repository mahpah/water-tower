const config = {
  host: 'localhost',
  port: 28015,
  db: 'test',
};

export default config;