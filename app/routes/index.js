import router from 'koa-router';
import views from 'co-views';
import path from 'path';
import authenticate from './api/authenticate';

let route = router();
authenticate(route);

/**
 * Render index
 */
let render = views(path.resolve(__dirname, '../views'), {default: 'jade'});
route.get('/', function*() {
  let html = yield render('index');
  this.body = html;
});

route.get('/date', function*() {
  this.body = new Date();
});

/**
 * prevent losing context
 */
export default route.routes.bind(route);
