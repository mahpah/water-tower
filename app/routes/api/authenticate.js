import jwt from 'jsonwebtoken';
import {TokenKey} from '../../config/key';
import {User} from './../../models/user.js';

function* validate(username, password) {
  if (!username || !password) {
    return false;
  }

  let user = yield User.findByUsername(username);
  if (!user) {
    return false;
  }

  let isUserValid = yield user.isPassword(password);
  return isUserValid ? user : false;
}

export default function(route) {
  route.all('/authenticate', function* () {
    let {username, password} = this.request.body;
    let claim;
    let user = yield validate(username, password);

    if (user) {
      claim = {
        id: user.id,
        username: user.username,
      };
      this.body = {
        access_token: jwt.sign(claim, TokenKey),
        username: user.username,
        id: user.id,
      };
      return;
    }

    this.status = 403;
    this.body = {
      message: 'Wrong username or password',
    };
    return;
  });

  route.get('/me', function* () {

    this.body = {};
  });
}
