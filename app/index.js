import koa from 'koa';
import { responseTime } from './libs/response-time.js';
import routes from './routes';
import parser from 'koa-bodyparser';

let app = koa();
app.use(responseTime('Koa-Response-Time'));
app.use(parser());
app.use(routes());

// export for testing
export default app;
