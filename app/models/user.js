import r from './../libs/rethink.js';
import bcrypt from 'co-bcrypt';
export const TableName = 'users';

export class User {
  constructor(params) {
    if (params && typeof params === 'object') {
      Object.assign(this, params);
    }
  }

  *save() {
    yield this._hashPassword();
    let data = this._validate();
    let result;
    if (!data) {
      return;
    }

    if (this.id) {
      result = yield r.table(TableName)
        .get(this.id)
        .update(data)
        .run();
    } else {
      result = yield r.table(TableName)
        .insert(data)
        .run();
      if (result && result.inserted === 1) {
        this.id = result.generated_keys[0];
      }
    }

    return result;
  }

  _validate() {
    const schema = {
      username: 'string',
      password: 'string',
    };

    let isValid = true;
    let sanitized = {};

    Object.keys(schema).forEach(key => {
      let type = schema[key];
      if (!this[key] || typeof this[key] !== type) {
        isValid = false;
      } else {
        sanitized[key] = this[key];
      }
    });

    if (isValid) {
      return sanitized;
    }

    return null;
  }

  static *findByUsername(username) {
    let result = yield r.table(TableName)
      .filter({username})
      .run();

    if (result && result.length === 1) {
      return new User(result[0]);
    }
  }

  *_hashPassword() {
    if (this._newPassword) {
      let salt = yield bcrypt.genSalt(10);
      this.password = yield bcrypt.hash(this.password, salt);
      this._newPassword = false;
    }
  }

  /**
   * mark new password when set password.
   * Use this trick in order to prevent hash function to be called
   * everytime user is saved
   */
  get password() {
    return this._password;
  }

  set password(value) {
    this._password = value;
    this._newPassword = true;
  }

  *isPassword(value) {
    return yield bcrypt.compare(value, this.password);
  }

  static *remove(id) {
    let result = yield r.table(TableName)
      .get(id)
      .delete()
      .run();
    return result ? result.deleted === 1 : false;
  }

  static *get(id) {
    let result = yield r.table(TableName)
      .get(id)
      .run();
    if (result) {
      return new User(result);
    }
  }
}

