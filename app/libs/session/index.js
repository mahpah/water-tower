import uuid from 'node-uuid';
import * as store from './session-store';

export default function sessionFactory(options) {
  //let store = {};
  let cookieName = 'koa.sid';
  let cookieOptions = {
    httpOnly: true,
    path: '/',
    overwrite: true,
    signed: true,
    maxAge: 864e5,
  };
  let isMigrated = false;

  function *loadToken(ctx) {
    let token = ctx.cookies.get(cookieName);
    if (token) {
      ctx.session = yield store.findByToken(token);
    }

    ctx.session = ctx.session || {};
    return token;
  }

  function *saveSession(ctx, token) {
    let isNew = false;
    if (!token) {
      isNew = true;
      token = uuid.v1();
    }

    ctx.cookies.set(cookieName, token, cookieOptions);
    if (ctx.session) {
      if (isNew) {
        yield store.add(token, ctx.session);
      } else {
        yield store.update(token, ctx.session);
      }
    }

    if (!ctx.session) {
      //delete store[token];
      yield store.remove(token);
    }
  }

  return function *session(next) {
    if (!isMigrated) {
      yield store.tryMigrate();
      isMigrated = true;
    }

    let token = yield loadToken(this);
    yield next;
    yield saveSession(this, token);
  };
}
