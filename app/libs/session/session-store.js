import dash from 'rethinkdbdash';
const DBName = 'sessiondb';
const TableName = 'session';
const TokenFieldName = 'token';
const r = dash({db: DBName});

export function* add(token, session) {
  session[TokenFieldName] = token;
  return yield r.table(TableName)
    .insert(session)
    .run();
}

export function* findByToken(token) {
  let result = yield r.table(TableName)
    .filter({
      [TokenFieldName]: token,
    })
    .run();

  if (result && result.length === 1) {
    return result[0];
  }
}

export function* update(token, session) {
  let result = yield r.table(TableName)
    .filter({
      [TokenFieldName]: token,
    })
    .update(session)
    .run();

  return result;
}

export function* remove(token) {
  return yield r.table(TableName)
    .filter({
      [TokenFieldName]: token,
    })
    .delete()
    .run();
}

export function* tryMigrate() {
  try {
    yield r.dbCreate(DBName).run();
    yield r.tableCreate(TableName).run();
    yield r.table(TableName).indexCreate(TokenFieldName).run();
  } catch (e) {
    console.log('ERROR: migrate database error');
    console.log(e);
    console.log('==============================');
  }
}
