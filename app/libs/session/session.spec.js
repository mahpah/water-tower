import 'babel-polyfill';
import request from 'co-supertest';
import koa from 'koa';
import session from './session';
import 'co-mocha';

const app = koa();
app.keys = ['ahihi'];

app.use(session());

app.use(function*() {
  switch (this.request.url) {
    case '/setname':
      this.session.userName = 'Ha Pham';
      this.body = this.session.userName;
      return;

    case '/getname':
      this.body = this.session.userName;
      return;

    case '/clear':
      this.session = null;
      this.status = 204;
      return;
  }
});

let server = app.listen();

/**
 * Test case
 */

describe('Session middleware', () => {

  describe('Session store can value', () => {
    let agent;

    before(() => {
      agent = request.agent(server);
    });

    it('should set name in session object', function* () {
      yield agent
        .get('/setname')
        .expect(200)
        .end();
    });
  });

  describe('Session can retrieve value', () => {
    let agent;

    before(function* () {
      agent = request.agent(server);
      yield agent
        .get('/setname')
        .expect(200)
        .end();
    });

    it('should retrieve session value', function*() {
      yield agent
        .get('/getname')
        .expect('Ha Pham')
        .end();
    });
  });

  describe('Session can be clear', () => {
    let agent;

    before(function* () {
      agent = request.agent(server);
      yield agent
        .get('/setname')
        .expect(200)
        .end();

    });

    it('should retrieve null session', function*() {
      yield agent
        .get('/getname')
        .expect('Ha Pham')
        .end();

      yield agent
        .del('/clear')
        .expect(204)
        .end();

      yield agent
        .get('/getname')
        .expect('')
        .end();
    });

  });
});
